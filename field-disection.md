

might be uniquie to oo designs

i have a record R

it's made of fields F0, F1, etc

----

if i can process/encode/decode fields, i can do the same for R

it's better for generated code

as i'll show later; i can seperate decomposition form encoding

still doesn't quite work

----

but ... but how?

val memberR; member[R] =
	for {
		f0 <- field[Float]("f0", _.f0)
		f1 <- field[String]("f1", _.f1)
		f2 <- field[Int]("f2", _.f2)
	} yield {
		R(f0, f1, f2)
	}

----

... which could have been ...

val memberR; member[R] =
	do {
		f0 <- field[Float]("f0", _.f0)
		f1 <- field[String]("f1", _.f1)
		f2 <- field[Int]("f2", _.f2)

		pure R(f0, f1, f2)
	}

----

`field` relies upon sugar like this ...

def field[T: member](key: String, get: R => T) =
	new {
		def flatMap(f: T => member[R]): member[R]
		def map(f: T => R): member[R]
	}

----

... which should have been ...

def field[T: member](key: String, get: R => T) =
	new {
		def bind(f: T => member[R]): member[R]
		def pure(f: T => R): member[R]
	}

----

... which i implement ase ...

case class Bind(key: String, get: R => T, f: T => member[R])
	extends member[R]

case class Pure(key: String, get: R => T, f: T => R)
	extends member[R]

----

... and we can then "unpack" a value

def unpack[R: member](root: R) = {
	def loop[T]: member[T] => ??? = {
		case Bind(key: String, get: R => T, f: T => member[R]) =>
			???

		case Pure(key: String, get: R => T, f: T => R) =>
			???
		}

	???
}

----

... but ...

I want to "encode" these now

I want to add a ...

TEncoder[T] {
	def put[F](old: T, m: member[F], v: F): T
}

----

... but ... but I can't - right?

i have to do a hash lookup

i also need a "seeb" value to decode

----

maybe i can do "trait inversion"

put enc/dec stuff as abstract

put all else as trait

require mixin extensions as implementation

----

yo - this works

i'm iterating on the design, but, it does that cool function wrapping thing to transform types and produce new feature
