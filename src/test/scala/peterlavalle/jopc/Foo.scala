package peterlavalle.jopc

case class Foo(i: Int, f: Float)

object Foo {

	trait dez[Z] extends Dez[Z] {

		implicit def dezInt: De[Int]

		implicit def dezFloat: De[Float]

		implicit val dezFoo: DeRecord[Foo] =
			for {
				i <- mem("i", (_: Foo).i)
				f <- mem("f", (_: Foo).f)
			} yield {
				for {
					i <- i
					f <- f
				} yield {
					Foo(i, f)
				}
			}
	}

}
