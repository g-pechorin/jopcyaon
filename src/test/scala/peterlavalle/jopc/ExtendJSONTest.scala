package peterlavalle.jopc

import org.json.{JSONArray, JSONException, JSONObject}
import org.scalatest.funsuite.AnyFunSuite
import peterlavalle.{TRandomTest, TTest}

class ExtendJSONTest extends TTest     {


	test("object ext array") {
		val jsonObject = new JSONObject()

		assert(!jsonObject.has("foo"))

		val now: JSONArray = jsonObject.extJSONArray("foo")
		assert(null != now)

		assert(jsonObject.has("foo"))

		assert(now eq jsonObject.extJSONArray("foo"))
	}
	test("object ext! array") {
		val jsonObject = new JSONObject()

		assert(!jsonObject.has("foo"))

		val caught =
			assertThrows[JSONException] {
				assert(null == jsonObject.extJSONArray("foo", null))
			}

		assert(!jsonObject.has("foo"))
	}
	test("object ext object") {
		val jsonObject = new JSONObject()

		assert(!jsonObject.has("foo"))

		val now: JSONObject = jsonObject.extJSONObject("foo")
		assert(null != now)

		assert(jsonObject.has("foo"))

		assert(now eq jsonObject.extJSONObject("foo"))
	}
	test("object ext! object") {
		val jsonObject = new JSONObject()

		assert(!jsonObject.has("foo"))

		val caught =
			assertThrows[JSONException] {
				assert(null == jsonObject.extJSONObject("foo", null))
			}

		assert(!jsonObject.has("foo"))
	}

	test("object / object") {
		val jsonObject = new JSONObject()

		assert(!jsonObject.has("foo"))

		val now: JSONObject = jsonObject / "foo"
		assert(null != now)

		assert(jsonObject.has("foo"))

		assert(now eq (jsonObject / "foo"))
	}

	test("object // object") {
		val jsonObject = new JSONObject()

		assert(!jsonObject.has("foo"))

		val now: JSONObject = jsonObject / "foo/bar"
		assert(null != now)

		assert(jsonObject.has("foo"))

		assert(now eq (jsonObject / "foo/bar"))
	}
}
