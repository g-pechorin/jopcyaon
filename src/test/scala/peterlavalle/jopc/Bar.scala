package peterlavalle.jopc

case class Bar(
								h: Short,
								s: Stream[String],
								f: Set[Foo],
								q: Set[Foo],
								n: Map[Short, Byte],
							)

object Bar {

	trait dez[Z] extends Dez.Atomic[Z]
		with Dez.Ext[Z]
		with Foo.dez[Z] {
		implicit def dezBar: DeRecord[Bar] = {
			for {
				h <- mem("h", (_: Bar).h)
				s <- box("s", (_: Bar).s)
				f <- arr("f", (_: Bar).f.toSeq.sortBy(_.toString))
				q <- set("q", (_: Bar).q)
				n <- dic("n", (_: Bar).n)
			} yield for {
				h <- h
				s <- s
				f <- f
				q <- q
				n <- n
			} yield {
				Bar(
					h,
					s.toStream,
					f.toSet,
					q,
					n,
				)
			}
		}
	}

}
