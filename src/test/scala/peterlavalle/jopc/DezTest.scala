package peterlavalle.jopc

import peterlavalle.TTest

import java.util.Random

/**
 * this class is the foundation of testing Dez - it tests random generation and (therefore) requires some functionality.
 *
 * encoders can't be tested this way ... sorry
 */
class DezTest extends TTest {
	private val count = 4

	test("the most basic test") {


		///
		// here's the Foo thing on its own
		// note how it needs these two things, but, doesn't need to assume anything about them
		trait DezFoo[Z] extends Dez[Z] {

			implicit def dezInt: De[Int]

			implicit def dezChar: De[Char]

			implicit val dezFoo: DeRecord[Foo] =
				for {
					a <- mem("a", (_: Foo).a)
					f <- mem("f", (_: Foo).f)
				} yield {
					for {
						a <- a
						f <- f
					} yield {
						Foo(a, f)
					}
				}
		}

		///
		// Foo is my/our dumb class
		case class Foo(a: Int, f: Char)


		//
		// we've defined the things - so let's test them
		//
		val test =
		new DezRandom(268)
			with DezFoo[Random]
		import test._
		val instance =
			test.decode[Foo](new Random())

		assert(instance != null)
	}

	test("test nesting objects") {
		case class Inner(i: Double)
		case class Outer(i: Inner, f: String)

		trait dez[Z] extends Dez[Z] {

			implicit def dezString: De[String]

			implicit def dezDouble: De[Double]

			implicit lazy val dezInner: DeRecord[Inner] =
				for {
					i <- mem("i", (_: Inner).i)
				} yield {
					for {
						i <- i
					} yield {
						Inner(i)
					}
				}

			implicit lazy val dezOuter: DeRecord[Outer] =
				for {
					i <- mem("i", (_: Outer).i)
					f <- mem("f", (_: Outer).f)
				} yield {
					for {
						i <- i
						f <- f
					} yield {
						Outer(i, f)
					}
				}
		}

		val test =
			new DezRandom(268)
				with dez[Random]

		import test._

		assert(test.decode[Outer](new Random()) != null)
		assert(test.decode[Inner](new Random()) != null)
	}

	test("test a sequence") {
		case class Inner(i: Double)
		case class Outer(i: Set[Inner], f: String)

		trait dez[Z] extends Dez[Z] {

			implicit def dezString: De[String]

			implicit def dezDouble: De[Double]

			implicit lazy val dezInner: DeRecord[Inner] =
				for {
					i <- mem("i", (_: Inner).i)
				} yield {
					for {
						i <- i
					} yield {
						Inner(i)
					}
				}
			implicit lazy val dezOuter: DeRecord[Outer] =
				for {
					i <- arr("i", (_: Outer).i.toSeq)
					f <- mem("f", (_: Outer).f)
				} yield {
					for {
						i <- i
						f <- f
					} yield {
						Outer(i.toSet, f)
					}
				}
		}

		val test =
			new DezRandom(268)
				with dez[Random]

		import test._

		assert(test.decode[Outer](new Random()) != null)
		assert(test.decode[Inner](new Random()) != null)
	}

	test("alternatives") {

		trait Base

		case class Fame(s: String) extends Base

		case class Count(q: Long, f: Double) extends Base

		trait More extends Base

		case class Beyond(evil: Boolean) extends More

		case class Mark(c: Char) extends More


		case class Container(
													aa: Base,
													qq: More,

													zz: Seq[Beyond],

													ww: Seq[Base],
												)


		trait dez[Z] extends Dez.Atomic[Z] with Dez.Alt[Z] {
			implicit lazy val dezContainer: DeRecord[Container] =
				for {
					aa <- mem("aa", (_: Container).aa)
					qq <- mem("qq", (_: Container).qq)
					zz <- arr("zz", (_: Container).zz)
					ww <- arr("ww", (_: Container).ww)
				} yield {
					for {
						aa <- aa
						qq <- qq
						zz <- zz
						ww <- ww
					} yield {
						Container(aa, qq, zz, ww)
					}
				}

			implicit lazy val dezBase: DeRecord[Base] =
				alt[Base](
					(_)
						.or {
							for {
								// this is copied because I CBA with variance
								s <- mem("s", (_: Fame).s)
							} yield {
								for {
									s <- s
								} yield {
									Fame(s)
								}
							}
						}
				)

			implicit val dezBeyond: DeRecord[Beyond] = for {
				s <- mem("s", (_: Beyond).evil)
			} yield {
				for {
					s <- s
				} yield {
					Beyond(s)
				}
			}
			implicit lazy val dezMore: DeRecord[More] = {

				alt[More] {
					(_)
						.or(dezBeyond)
						.or(
							for {
								s <- mem("s", (_: Mark).c)
							} yield {
								for {
									s <- s
								} yield {
									Mark(s)
								}
							}
						)
				}
			}
		}

		val rand =
			new DezRandom(213)
				with dez[Random]

		(0 until count)
			.foreach {
				_ =>
					val container = rand.dezContainer.decode(new Random())
					require(null != container)
			}
	}

	test("crash Random") {

		val rand = new DezRandom(314) with Bar.dez[Random] with Foo.dez[Random] with Goo.dez[Random]

		import rand._

		def test[T: DeRecord](): Unit = {
			val instance = decode[T](new Random())
			assertThrows[RandomCantEncodeException] {
				encode(instance)
			}
		}

		test[Foo]()
		test[Bar]()
		test[Goo]()
	}

	test("IR with alts") {

		trait Car

		case class Kia(name: String) extends Car

		case class Ford(rob: Short) extends Car

		case class Lexus() extends Car
		case object Toyota extends Car

		trait dez[Z] {
			this: Dez.Alt[Z] with Dez.Atomic[Z] =>
			implicit val dezCar: DeRecord[Car] =
				alt[Car](
					(_: alt.alt[Car])
						.or(Toyota)
						.or(Lexus())
						.or {
							for {
								rob <- mem("rob", (_: Ford).rob)
							} yield {
								for {
									rob <- rob
								} yield {
									Ford(rob)
								}
							}
						}
						.or {
							for {
								name <- mem("name", (_: Kia).name)
							} yield {
								for {
									name <- name
								} yield {
									Kia(name)
								}
							}
						}
				)
		}

		val dir = new Dez.DezIR with dez[Dez.IR]

		import dir._
		val ir: Dez.IR = dir.ir[Car]

		implicit class extendedStreamCon[A](head: A) {
			def ::#(tail: => Iterable[A]): Stream[A] = Stream.cons(head, tail.toStream)
		}

		val lines: Stream[String] =
			ir.lines {
				(f: Dez.IR => Stream[String]) => {
					case de: Dez.IR.Atomic[_, _] =>
						List(de.de.toString)

					case Dez.IR.Record(name: String, args: List[(String, Dez.IR)]) =>
						(name + "=") ::# args.flatMap {
							case (name, ir) =>
								(name + ":") #:: f(ir).map("\t" + (_: String))
						}.map("\t" + _)

					case Dez.Alt.Switch(name: String, bs: List[Dez.Alt.Branch[_]]) =>
						(name + "/") ::# {
							bs.zipWithIndex.flatMap {
								case (item, index) =>
									("#" + index) #:: (item match {
										case Dez.Alt.C(k, _) => Stream("(constant: " + k.getSimpleName + ")")
										case Dez.Alt.R(r) => f(r)
									}).map("\t" + _)
							}.map("\t" + _)
						}
				}
			}


		assertSourceEqual(
			actual = lines.foldLeft("")(_ + _ + "\n").trim,
			expected =
				"""
					|Car$1/
					|	#0
					|		(constant: Toyota$1$)
					|	#1
					|		(constant: Lexus$1)
					|	#2
					|		Ford$1=
					|			rob:
					|				atomic@short
					|	#3
					|		Kia$1=
					|			name:
					|				atomic@java.lang.String
					|
				""".stripMargin.trim
		)
	}
}
