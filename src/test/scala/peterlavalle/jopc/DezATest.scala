package peterlavalle.jopc

import peterlavalle.TTest

import java.util.Random


abstract class DezATest[Z](of: => Dez.Atomic[Z] with Dez.Alt[Z]) extends TTest {
	lazy val quirk: Dez.Alt[Z] with Dez.Atomic[Z] with Foo.dez[Z] with Bar.dez[Z] = {

		// need to "stabilize" the value
		val stable: Dez.Atomic[Z] with Dez.Alt[Z] = of

		new stable.AltChild with stable.AtomicChild
			with Foo.dez[Z] with Bar.dez[Z]
	}
	val range: Int = env("dez.test.range", 19).toInt
	val count: Int = env("dez.test.count", 38).toInt

	private val rand =
		new DezRandom(range)
			with Foo.dez[Random]
			with Bar.dez[Random]

	//	private val random = new Random()

	test("random Foo") {
		seeded {
			random =>
				import rand._
				decode[Foo](random)
		}
	}

	test("random Bar") {
		seeded {
			random =>
				import rand._
				decode[Bar](random)
		}
	}


	test("encode random Foo values") {
		seeded {
			random =>

				// generate
				val src: List[Foo] =
					(0 until count)
						.map {
							_: Int =>
								import rand._
								decode[Foo](random)
						}
						.toList

				// encode
				// flip the buffer to be read-from
				val dat: List[Z] = {

					import quirk._

					src
						.map(encode[Foo])
				}

				assert(dat.size == count)
		}
	}

	test("encode random Bar values") {
		seeded {
			random =>
				// generate
				val src: List[Bar] =
					(0 until count)
						.map {
							i =>
								import rand._
								decode[Bar](random)
						}
						.toList

				// encode
				val dat: List[Z] = {
					import quirk._
					src
						.map(encode[Bar])
				}

				assert(dat.size == count)
		}
	}

	test("encode/decode random Foo values") {
		seeded {
			random =>

				// generate
				val src: List[Foo] =
					(0 until count)
						.map {
							_: Int =>
								import rand._
								decode[Foo](random)
						}
						.toList

				// encode
				// flip the buffer to be read-from
				val dat: List[Z] = {

					import quirk._

					src
						.map(encode[Foo])
				}

				// decode and test
				(src zip dat).foreach {
					case (expected: Foo, dat) =>

						val actual = {
							import quirk._
							decode[Foo](dat)
						}

						assert(actual == expected)
				}
		}
	}

	test("encode/decode random Bar values") {
		seeded {
			random =>
				// generate
				val src: List[Bar] =
					(0 until count)
						.map {
							i =>
								import rand._
								decode[Bar](random)
						}
						.toList

				// encode
				val dat: List[Z] = {
					import quirk._
					src
						.map(encode[Bar])
				}

				// decode and test
				(src zip dat).foreach {
					case (expected: Bar, dat) =>

						val actual = {
							import quirk._
							decode[Bar](dat)
						}

						assert(actual == expected)
				}
		}
	}

	test("test nesting objects") {
		seeded {
			random =>

				case class Inner(i: Double)
				case class Outer(i: Inner, f: String)

				trait dez[Z] extends Dez[Z] {

					implicit def dezString: De[String]

					implicit def dezDouble: De[Double]

					implicit lazy val dezInner: DeRecord[Inner] =
						for {
							i <- mem("i", (_: Inner).i)
						} yield {
							for {
								i <- i
							} yield {
								Inner(i)
							}
						}

					implicit lazy val dezOuter: DeRecord[Outer] =
						for {
							i <- mem("i", (_: Outer).i)
							f <- mem("f", (_: Outer).f)
						} yield {
							for {
								i <- i
								f <- f
							} yield {
								Outer(i, f)
							}
						}
				}

				val quirk = new this.quirk.AltChild
					with dez[Z] {
					override implicit val dezString: De[String] = include(DezATest.this.quirk.dezString)
					override implicit val dezDouble: De[Double] = include(DezATest.this.quirk.dezDouble)
				}

				val rand = new DezATest.this.rand.AltChild
					with dez[Random] {
					override implicit val dezString: De[String] = include(DezATest.this.rand.dezString)
					override implicit val dezDouble: De[Double] = include(DezATest.this.rand.dezDouble)
				}

				// generate
				val src: List[Outer] =
					(0 until count)
						.map {
							_: Int =>
								import rand._
								decode[Outer](random)
						}
						.toList

				// encode
				val dat: List[Z] = {

					import quirk._

					src
						.map(encode[Outer])
				}

				// decode and test
				(src zip dat).foreach {
					case (expected: Outer, dat) =>

						val actual = {
							import quirk._
							decode[Outer](dat)
						}

						assert(actual == expected)
				}
		}
	}

	test("test a sequence") {
		seeded {
			random =>
				case class Inner(i: Double)
				case class Outer(i: Set[Inner], f: String)

				trait dez[Z] extends Dez[Z] {

					implicit def dezString: De[String]

					implicit def dezDouble: De[Double]

					implicit lazy val dezInner: DeRecord[Inner] =
						for {
							i <- mem("i", (_: Inner).i)
						} yield {
							for {
								i <- i
							} yield {
								Inner(i)
							}
						}
					implicit lazy val dezOuter: DeRecord[Outer] =
						for {
							i <- arr("i", (_: Outer).i.toSeq)
							f <- mem("f", (_: Outer).f)
						} yield {
							for {
								i <- i
								f <- f
							} yield {
								Outer(i.toSet, f)
							}
						}
				}

				val quirk = new DezATest.this.quirk.AltChild
					with dez[Z] {
					override implicit val dezString: De[String] = include(DezATest.this.quirk.dezString)
					override implicit val dezDouble: De[Double] = include(DezATest.this.quirk.dezDouble)
				}

				val rand = new DezATest.this.rand.AltChild
					with dez[Random] {
					override implicit val dezString: De[String] = include(DezATest.this.rand.dezString)
					override implicit val dezDouble: De[Double] = include(DezATest.this.rand.dezDouble)
				}

				// generate
				val src: List[Outer] =
					(0 until count)
						.map {
							_: Int =>
								import rand._
								decode[Outer](random)
						}
						.toList

				// encode
				val dat: List[Z] = {

					import quirk._

					src
						.map(encode[Outer])
				}

				// decode and test
				(src zip dat).foreach {
					case (expected: Outer, dat) =>

						val actual = {
							import quirk._
							decode[Outer](dat)
						}

						assert(actual == expected)
				}
		}
	}

	test("alternatives") {
		seeded {
			random =>

				trait Base

				case class Fame(s: String) extends Base

				case class Count(q: Long, f: Double) extends Base

				trait More extends Base

				case class Beyond(evil: Boolean) extends More

				case class Mark(c: Char) extends More

				case class Container(
															aa: Base,
															qq: More,

															zz: Seq[Beyond],

															ww: Seq[Base],
														)


				trait dez[Z] extends Dez.Atomic[Z] with Dez.Alt[Z] {
					implicit lazy val dezContainer: DeRecord[Container] =
						for {
							aa <- mem("aa", (_: Container).aa)
							qq <- mem("qq", (_: Container).qq)
							zz <- arr("zz", (_: Container).zz)
							ww <- arr("ww", (_: Container).ww)
						} yield {
							for {
								aa <- aa
								qq <- qq
								zz <- zz
								ww <- ww
							} yield {
								Container(aa, qq, zz, ww)
							}
						}

					implicit lazy val dezBase: DeRecord[Base] =
						alt[Base](
							(_)
								.or {
									for {
										// this is copied because I CBA with variance
										s <- mem("s", (_: Fame).s)
									} yield {
										for {
											s <- s
										} yield {
											Fame(s)
										}
									}
								}
						)

					implicit val dezBeyond: DeRecord[Beyond] = for {
						s <- mem("s", (_: Beyond).evil)
					} yield {
						for {
							s <- s
						} yield {
							Beyond(s)
						}
					}
					implicit lazy val dezMore: DeRecord[More] =
						alt[More] {
							(_: alt.alt[More])
								.or(dezBeyond)
								.or(
									for {
										s <- mem("s", (_: Mark).c)
									} yield {
										for {
											s <- s
										} yield {
											Mark(s)
										}
									}
								)
						}
				}

				//
				// with all that defined we can begin the testing
				//

				val quirk = new DezATest.this.quirk.AtomicChild with DezATest.this.quirk.AltChild
					with dez[Z]

				val rand = new DezATest.this.rand.AtomicChild with DezATest.this.rand.AltChild
					with dez[Random]

				// generate
				val src: List[Container] =
					(0 until count)
						.map {
							_: Int =>
								import rand._
								decode[Container](random)
						}
						.toList

				// encode
				val dat: List[Z] = {

					import quirk._

					src
						.map(encode[Container])
				}

				// decode and test
				(src zip dat).foreach {
					case (expected: Container, dat) =>

						val actual = {
							import quirk._
							decode[Container](dat)
						}

						assert(actual == expected)
				}

		}
	}

	test("alts with the case one") {
		seeded {
			random =>

				trait Base

				case class Fame(s: String) extends Base

				case class Count(q: Long, f: Double) extends Base

				case class Note() extends Base

				trait dez[Z] extends Dez.Atomic[Z] with Dez.Alt[Z] {
					implicit lazy val dezBase: DeRecord[Base] =
						alt[Base](
							(_)
								.or(Note())
								.or {
									for {
										// this is copied because I CBA with variance
										s <- mem("s", (_: Fame).s)
									} yield {
										for {
											s <- s
										} yield {
											Fame(s)
										}
									}
								}
								.or {
									dezCount
								}
						)

					implicit val dezCount: DeRecord[Count] = for {
						q <- mem("q", (_: Count).q)
						f <- mem("f", (_: Count).f)
					} yield {
						for {
							q <- q
							f <- f
						} yield {
							Count(q, f)
						}
					}
				}

				//
				// with all that defined we can begin the testing
				//

				val quirk = new DezATest.this.quirk.AtomicChild with DezATest.this.quirk.AltChild
					with dez[Z]

				val rand = new DezATest.this.rand.AtomicChild with DezATest.this.rand.AltChild
					with dez[Random]

				// generate
				val src: List[Base] =
					(0 until count)
						.map {
							_: Int =>
								import rand._
								decode[Base](random)
						}
						.toList

				// encode
				val dat: List[Z] = {

					import quirk._

					src
						.map(encode[Base])
				}

				// decode and test
				(src zip dat).foreach {
					case (expected: Base, dat) =>

						val actual = {
							import quirk._
							decode[Base](dat)
						}

						assert(actual == expected)
				}
		}

	}

	test("alts only the case one") {
		seeded {
			random =>
				trait Base

				case class Fame() extends Base

				case object Count extends Base

				case class Note() extends Base

				trait dez[Z] extends Dez.Atomic[Z] with Dez.Alt[Z] {
					implicit lazy val dezBase: DeRecord[Base] =
						alt[Base](
							(_)
								.or(Note())
								.or(Fame())
								.or(Count)
						)
				}

				//
				// with all that defined we can begin the testing
				//

				val quirk = new DezATest.this.quirk.AtomicChild with DezATest.this.quirk.AltChild
					with dez[Z]

				val rand = new DezATest.this.rand.AtomicChild with DezATest.this.rand.AltChild
					with dez[Random]

				// generate
				val src: List[Base] =
					(0 until count)
						.map {
							_: Int =>
								import rand._
								decode[Base](random)
						}
						.toList

				// encode
				val dat: List[Z] = {

					import quirk._

					src
						.map(encode[Base])
				}

				// decode and test
				(src zip dat).foreach {
					case (expected: Base, dat) =>

						val actual = {
							import quirk._
							decode[Base](dat)
						}

						assert(actual == expected)
				}

		}
	}

	test("optionals") {

		case class Dope(n: String, f: Option[Int])

		trait dez[Z] extends Dez.Alt[Z] with Dez.Atomic[Z] {
			implicit val dezDope =
				for {
					n <- mem("n", (_: Dope).n)
					f <- opt("f", (_: Dope).f)
				} yield for {
					n <- n
					f <- f
				} yield Dope(n, f)
		}


		val quirk = new this.quirk.AltChild
			with this.quirk.AtomicChild
			with dez[Z]

		val rand = new DezATest.this.rand.AltChild
			with this.rand.AtomicChild
			with dez[Random]

		seeded {
			random =>
				// generate
				val src: List[Dope] =
					(0 until count)
						.map {
							_: Int =>
								import rand._
								decode[Dope](random)
						}
						.toList
				import quirk._

				// encode
				val dat: List[Z] = {
					src
						.map(encode[Dope])
				}

				// decode and test
				(src zip dat).foreach {
					case (expected: Dope, dat) =>
						assert(decode[Dope](dat) == expected)
				}
		}
	}

	test("encode/decode random Goo values") {
		seeded {
			random =>

				val quirk = new this.quirk.AltChild
					with this.quirk.AtomicChild
					with Goo.dez[Z]

				val rand = new DezATest.this.rand.AltChild
					with this.rand.AtomicChild
					with Goo.dez[Random]


				// generate
				val src: List[Goo] =
					(0 until count)
						.map {
							_: Int =>
								import rand._
								decode[Goo](random)
						}
						.toList

				// encode
				// flip the buffer to be read-from
				val dat: List[Z] = {

					import quirk._

					src
						.map(encode[Goo])
				}

				// decode and test
				(src zip dat).foreach {
					case (expected: Goo, dat) =>

						val actual = {
							import quirk._
							decode[Goo](dat)
						}

						assert(actual == expected)
				}
		}
	}

}
