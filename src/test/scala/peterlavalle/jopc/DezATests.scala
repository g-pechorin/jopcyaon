package peterlavalle.jopc

import org.json.JSONObject

class DezDataStreamTest
	extends DezATest[DataStream](new DezDataStream())


class DezJSONObjectTest
	extends DezATest[JSONObject](new DezJSONObject) {

	/**
	 * i had a lot of problems with char values, so, this ... "verifies" them
	 */
	test("test char put") {
		val data =
			new JSONObject()
				.put("s", 'a')
				.putChar("a", 's')

		assert('a'.toInt == data.getInt("s"))
		assert('s'.toInt == data.getChar("a"))
	}
}
