package peterlavalle.jopc

/**
 * it's a dumb variation on Bar but it lets me do more testing
 *
 * @param f
 * @param n
 */
case class Goo(
								f: Set[Short],
								n: Map[Foo, String],
							)

object Goo {

	trait dez[Z] extends Dez.Atomic[Z]
		with Foo.dez[Z]
		with Dez.Ext[Z] {
		implicit def dezGoo: DeRecord[Goo] = {
			for {
				n <- dic("n", (_: Goo).n)
				f <- boi("f", (_: Goo).f)
			} yield for {
				n <- n
				f <- f
			} yield {
				Goo(
					f,
					n,
				)
			}
		}
	}

}
