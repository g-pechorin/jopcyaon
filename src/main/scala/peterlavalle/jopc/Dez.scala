package peterlavalle.jopc

import peterlavalle.jopc.Dez.Alt.Branch

import scala.reflect.{ClassTag, classTag}

object Dez {

	final val altDataKey: String = ".data."

	trait IR {
		object lines {
			def apply(f: (IR => Stream[String]) => IR => Seq[String]): Stream[String] =
				f(_.lines(f))(IR.this).toStream
		}

		TODO("make this/a a thing into a semi-standard thing for generation")
	}

	trait Ext[Z] {
		this: Dez[Z] =>


		/**
		 * it's a key:value dictionary!
		 */
		def dic[O: ClassTag, K: De : ClassTag, V: De : ClassTag](key: String, dec: O => Map[K, V]): P[O, DeHandle[Map[K, V], O], DeFactory, DeRecord] = {

			implicit val dezItem: DeRecord[(K, V)] =
				for {
					key <- mem("key", (_: (K, V))._1)
					value <- mem("value", (_: (K, V))._2)
				} yield for {
					key <- key
					value <- value
				} yield (key, value)

			p(
				arr(key, (o: O) => (dec andThen ((_: Map[K, V]).toListBy((_: (K, V)).toString()).map {
					case (k, v) =>
						(k, v)
				})) (o))
			)(
				(s: Seq[(K, V)]) =>
					s.map((i: (K, V)) => i._1 -> i._2).toMap
			)
		}

		/**
		 * it's a set
		 */
		def set[O: ClassTag, V: De : ClassTag](key: String, dec: O => Set[V]): P[O, DeHandle[Set[V], O], DeFactory, DeRecord] = {

			case class Box(v: V)

			implicit val dezBox: DeRecord[Box] =
				for {
					v <- mem(classFor[T].getSimpleName, (_: Box).v)
				} yield for {
					v <- v
				} yield {
					Box(v)
				}

			p(arr(key, (o: O) => (dec andThen ((_: Set[V])
				.toListBy((_: V).toString())
				.map(Box))) (o)))(
				(_: Seq[Box])
					.map((i: Box) => i.v)
					.toSet
			)
		}
	}

	trait Alt[Z] extends Dez[Z] {

		private val _encLabel: (Z, List[label], Int) => Z = (z: Z, l: List[label], i: Int) => encLabel(z, l, i)
		private val _decLabel: (Z, List[label]) => (Z, Int) = (z: Z, l: List[label]) => decLabel(z, l)

		/**
		 * it's "optional" values - maybes and such
		 */
		def opt[O: ClassTag, V: De : ClassTag](key: String, dec: O => Option[V]): P[O, DeHandle[Option[V], O], DeFactory, DeRecord] = {
			implicit val maybe: DeRecord[Option[V]] =
				alt[Option[V]](
					(_: alt.alt[Option[V]])
						.or(None)
						.or {
							for {
								just <- mem("just", (_: Some[V]).get)
							} yield for {
								just <- just
							} yield {
								Some(just)
							}
						}
				)

			new P[O, DeHandle[Option[V], O], DeFactory, DeRecord] {
				override def map(f: DeHandle[Option[V], O] => DeFactory[O]): DeRecord[O] =
					mem(key, dec).map(f)

				override def flatMap(f: DeHandle[Option[V], O] => DeRecord[O]): DeRecord[O] =
					mem(key, dec).flatMap(f)
			}
		}

		def encLabel(z: Z, a: List[label], i: Int): Z

		def decLabel(z: Z, a: List[label]): (Z, Int)

		trait AltChild extends super.Child with Alt[Z] {
			override final def encLabel(z: Z, a: List[label], i: Int): Z = {
				base
					.asInstanceOf[Alt[Z]]
					._encLabel
					.asInstanceOf[(Z, List[label], Int) => Z]
					.apply(z, a, i)
			}

			override final def decLabel(z: Z, a: List[label]): (Z, Int) =
				base
					.asInstanceOf[Alt[Z]]
					._decLabel
					.asInstanceOf[(Z, List[label]) => (Z, Int)]
					.apply(z, a)
		}

		trait label {
			def name: String

			def record: Option[De[_]]
		}

		object alt {


			def apply[T: ClassTag](all: alt[T] => out[T]): DeRecord[T] = {

				/**
				 * sneaky abstraction - the "case options" can't provide a "full" De[Z] so we're wrapping what we need
				 */
				trait Dz[Q <: T] extends label {
					def name: String = kind.getName

					def encode(t: Q): Z

					def decode(z: Z): Q

					def kind: Class[_]
				}

				class Norm[Q <: T](val de: DeRecord[Q]) extends Dz[Q] {
					override def encode(t: Q): Z = de.encode(t)

					override def decode(z: Z): Q = de.decode(z)

					override def kind: Class[_] = de.kind

					override def record: Option[De[_]] = Some(de)

					def branch: Branch[T] =
						de.ir(de) match {
							case record: Dez.IR.Record => Alt.R(record)
						}
				}

				class Fake[Q <: T : ClassTag](val get: () => Q) extends Dz[Q] {
					override def encode(t: Q): Z = newZInstance()

					override def decode(z: Z): Q = get()

					override def kind: Class[_] = classFor[Q]

					override def record: Option[De[_]] = None

					def branch: Branch[T] = Alt.C(classFor[Q], get)
				}

				val list: List[Dz[_ <: T]] = {

					trait t extends out[T] {
						def all: List[Dz[_ <: T]]

						override def or[Q <: T](q: DeRecord[Q]): alt.out[T] =
							new chain(new Norm(q) :: all)

						override def or[Q <: T : ClassTag](q: => Q): out[T] =
							new chain(new Fake[Q](() => q) :: all)
					}

					class chain(val all: List[Dz[_ <: T]]) extends t

					all(
						new t {
							override val all: List[Dz[_ <: T]] = Nil
						}
					).asInstanceOf[chain]
						// don't sort these - that'd be weird
						.all.reverse
				}

				val used: Set[De[_]] =
					list.flatMap {
						(_: Dz[_]) match {
							case normal: Norm[_] =>
								List[De[_]](normal.de)
							case _: Fake[_] =>
								Nil
						}
					}.toSet

				DeRecord[T](
					(z: Z, t: T) => {

						def select(value: T): (Int, Dz[T]) =
							list
								.zipWithIndex
								.filter((_: (Dz[_ <: T], Int))._1.kind.isInstance(value)) match {
								case List((dz, index)) =>
									index -> {
										// have to cast because of erasure
										dz.asInstanceOf[Dz[T]]
									}
							}

						val (idx, de) = select(t)
						val data: Z = de.encode(t)

						// create a blank container
						val z0: Z = newZInstance()

						// put the label into the container
						val z1: Z = encLabel(z0, list, idx)

						// embed the data
						val z2: Z = put(z1, altDataKey, data)

						// so that's it ... right?
						z2
					},

					used,

					DeFactory {
						(z0: Z) =>

							// pull the label out
							val (z1, kind) = decLabel(z0, list)

							// debed the data
							val (z2, data) = get(z1, altDataKey)

							// decode the thing - ensure it's the only thing
							val self: T = list(kind).decode(data)

							// return the values
							z2 -> self
					}
				)(
					(de: DeRecord[T]) =>
						Dez.Alt.Switch(
							de.name,
							list.map {
								case norm: Norm[_] =>
									norm.branch
								case fake: Fake[_] =>
									fake.branch
							}
						)
				)
			}

			sealed trait alt[T] {
				def or[Q <: T](q: DeRecord[Q]): out[T]

				def or[Q <: T : ClassTag](q: => Q): out[T]
			}

			/**
			 * there's a shenanigan to force at least one instance
			 */
			sealed trait out[T] extends alt[T]

		}

	}

	trait Atomic[Z] extends Dez[Z] {
		implicit def dezShort: De[Short]

		implicit def dezString: De[String]

		implicit def dezBoolean: De[Boolean]

		implicit def dezInt: De[Int]

		implicit def dezFloat: De[Float]

		implicit def dezChar: De[Char]

		implicit def dezDouble: De[Double]


		implicit def dezByte: De[Byte]

		implicit def dezLong: De[Long]

		def box[O: ClassTag, T: De : ClassTag](key: String, dec: O => Seq[T]): P[O, DeHandle[Seq[T], O], DeFactory, DeRecord] = {

			case class Box(v: T)

			implicit val dezBox: DeRecord[Box] =
				for {
					v <- mem(classFor[T].getSimpleName, (_: Box).v)
				} yield for {
					v <- v
				} yield {
					Box(v)
				}

			val real: P[O, DeHandle[Seq[Box], O], DeFactory, DeRecord] = arr[O, Box](key, dec andThen ((_: Seq[T]).map(Box(_: T))))

			p(real)((_: Seq[Box]).map((_: Box).v))
		}

		def boi[O: ClassTag, T: De : ClassTag](key: String, dec: O => Set[T]): P[O, DeHandle[Set[T], O], DeFactory, DeRecord] = {

			case class Box(v: T)

			implicit val dezBox: DeRecord[Box] =
				for {
					v <- mem(classFor[T].getSimpleName, (_: Box).v)
				} yield for {
					v <- v
				} yield {
					Box(v)
				}

			val real: P[O, DeHandle[Seq[Box], O], DeFactory, DeRecord] =
				arr[O, Box](key, dec andThen ((_: Set[T]).toListBy((_: T).toString).map(Box(_: T))))

			p(real)((_: Seq[Box]).toSet.map((_: Box).v))
		}

		trait AtomicChild extends super.Child {
			implicit final lazy val dezString: De[String] = include(Atomic.this.dezString)
			implicit final lazy val dezDouble: De[Double] = include(Atomic.this.dezDouble)
			implicit final lazy val dezShort: De[Short] = include(Atomic.this.dezShort)
			implicit final lazy val dezBoolean: De[Boolean] = include(Atomic.this.dezBoolean)
			implicit final lazy val dezInt: De[Int] = include(Atomic.this.dezInt)
			implicit final lazy val dezFloat: De[Float] = include(Atomic.this.dezFloat)
			implicit final lazy val dezChar: De[Char] = include(Atomic.this.dezChar)
			implicit final lazy val dezByte: De[Byte] = include(Atomic.this.dezByte)
			implicit final lazy val dezLong: De[Long] = include(Atomic.this.dezLong)
		}

	}

	trait All[Z] extends Dez[Z] with Ext[Z] with Alt[Z] with Atomic[Z] {

		override final def encLabel(z: Z, a: List[label], i: Int): Z =
			error("do this in a standard way")

		override final def decLabel(z: Z, a: List[label]): (Z, Int) =
			error("do this in a standard way")

		override final protected def put(z: Z, k: String, v: Z): Z =
			put(z, k, Seq(v))

		override final protected def get(z: Z, k: String): (Z, Z) =
			seq(z, k)
				.mapr {
					case Seq(o) =>
						o
				}
	}

	class DezIR extends Dez.Atomic[IR] with Dez.Alt[IR] {

		override def put(invoice: IR, key: String, value: IR): IR = !!!

		override def put(invoice: IR, key: String, value: Seq[IR]): IR = !!!

		override def encLabel(z: IR, a: List[label], i: Int): IR = !!!

		override def decLabel(z: IR, a: List[label]): (IR, Int) = !!!

		@inline private final def !!! = error("don't try to encode/decode with this")

		override protected def get(z: IR, k: String): (IR, IR) = !!!

		override implicit val dezShort: De[Short] =
			deAtomic[Short](
				(_, _, _) => !!!
			)(
				(_, _) => !!!
			)

		override implicit val dezString: De[String] =
			deAtomic[String](
				(_, _, _) => !!!
			)(
				(_, _) => !!!
			)

		override implicit def dezBoolean: De[Boolean] = ???

		override implicit val dezInt: De[Int] =
			deAtomic[Int](
				(_, _, _) => !!!
			)(
				(_, _) => !!!
			)

		override implicit val dezFloat: De[Float] =
			deAtomic[Float](
				(_, _, _) => !!!
			)(
				(_, _) => !!!
			)

		override protected def seq(z: IR, k: String): (IR, Seq[IR]) = !!!

		override protected def eoz(z: IR): Boolean = !!!

		override implicit def dezChar: De[Char] = ???

		override implicit def dezDouble: De[Double] = ???

		override implicit def dezByte: De[Byte] = ???

		override implicit def dezLong: De[Long] =
			deAtomic[Long](
				(_, _, _) => !!!
			)(
				(_, _) => !!!
			)

		override protected def newZInstance(): IR = !!!


	}

	object Alt {

		sealed trait Branch[T]

		case class R[T](r: IR.Record) extends Branch[T]

		case class C[T](k: Class[_], v: () => T) extends Branch[T]

		case class Switch[T](name: String, bs: List[Branch[T]]) extends IR

	}

	object IR {

		case class Atomic[Z, T: ClassTag](de: Dez[Z]#DeAtomic[T]) extends IR {
			val kind = classTag[T].runtimeClass
		}

		case class Record(name: String, args: List[(String, IR)]) extends IR

	}

}

trait Dez[Z] {

	def encode[O: DeRecord](o: O): Z = implicitly[DeRecord[O]].encode(o)

	def decode[O: DeRecord](z: Z): O = implicitly[DeRecord[O]].decode(z)

	def mem[O: ClassTag, T: De : ClassTag](key: String, dec: O => T): P[O, DeHandle[T, O], DeFactory, DeRecord] =
		new DeField(key).field(dec)

	def arr[O: ClassTag, T: DeRecord : ClassTag](key: String, dec: O => Seq[T]): P[O, DeHandle[Seq[T], O], DeFactory, DeRecord] =
		new DeField(key).array(dec)


	def ir[T: DeRecord]: Dez.IR = {
		val de: DeRecord[T] = implicitly[DeRecord[T]]
		de.ir.apply(de)
	}

	def deAtomic[T: ClassTag](
														 e: (Z, String, T) => Z
													 )(
														 d: (Z, String) => (Z, T)
													 ): DeAtomic[T] =
		new DeAtomic[T] {
			override val toString: String = "atomic@" + classFor[T].getName

			override def apply(i: Z, key: String, t: T): Z = e(i, key, t)

			override def apply(z: Z, k: String): (Z, T) = d(z, k)

			override val uses: Set[De[_]] = Set()
		}

	protected def put(z: Z, k: String, v: Z): Z

	protected def put(z: Z, k: String, v: Seq[Z]): Z

	/**
	 * wrapper to convert something from/to an array
	 *
	 * ... it's how I do map/set/opt ...
	 */
	protected def p[Q, T, O](
														real: P[O, DeHandle[Q, O], DeFactory, DeRecord]
													)(
														into: Q => T
													): P[O, DeHandle[T, O], DeFactory, DeRecord] =
		new P[O, DeHandle[T, O], DeFactory, DeRecord] {
			private def impose(real: DeHandle[Q, O]): DeHandle[T, O] =
				new DeHandle[T, O] {
					override val key: String = real.key

					override def map(f: T => O): DeFactory[O] = real.map((h: Q) => f(into(h)))

					override def flatMap(f: T => DeFactory[O]): DeFactory[O] = real.flatMap((h: Q) => f(into(h)))
				}

			override def map(f: DeHandle[T, O] => DeFactory[O]): DeRecord[O] =
				real.map((real: DeHandle[Q, O]) => f(impose(real)))

			override def flatMap(f: DeHandle[T, O] => DeRecord[O]): DeRecord[O] =
				real.flatMap((real: DeHandle[Q, O]) => f(impose(real)))
		}

	/**
	 * dez: here's the secret - De2 and newField treated V differently.
	 *
	 * retrieves a sub-component from Z for the given field
	 *
	 * ... for JSON it would be the next Object
	 * ... for Random it could be a sub-Random?
	 * ... for bytes it's a block of byte lead by a 32bit size value. crude.
	 */
	protected def get(z: Z, k: String): (Z, Z)

	/**
	 * dez: here's another secret - sequences are treated really different AND can only be fat-records
	 */
	protected def seq(z: Z, k: String): (Z, Seq[Z])

	/**
	 * test if a container is "at end"
	 *
	 * used to verify that fields are decomposed properly - for Random and JSON just return true always
	 *
	 * ... i suppose we could erase JSON values as we read them ... which would do a nice checking. but it's bad because it would prevent wrong JSON ... aah ... wait; i guess that i shold do that.
	 */
	protected def eoz(z: Z): Boolean

	protected def newZInstance(): Z

	private def dez: Dez[Z] = this

	sealed trait DeAtomic[T] extends De[T] {
		def apply(i: Z, key: String, t: T): Z

		def apply(z: Z, k: String): (Z, T)
	}

	trait P[O, K, P[_], M[_]] {
		def map(f: K => P[O]): M[O]

		def flatMap(f: K => M[O]): M[O]
	}

	trait DeHandle[T, O] {
		def key: String

		def map(f: T => O): DeFactory[O]

		def flatMap(f: T => DeFactory[O]): DeFactory[O]
	}

	/**
	 * dez: should this be "dec?"
	 */
	sealed abstract class De[T: ClassTag] {
		val uses: Set[De[_]]
		val name: String = implicitly[ClassTag[T]].runtimeClass.getSimpleName
		val path: String = implicitly[ClassTag[T]].runtimeClass.getName
	}

	abstract class Child(val base: Dez[Z]) extends Dez[Z] {
		override protected final def get(z: Z, k: String): (Z, Z) = dez.get(z, k)

		override protected final def seq(z: Z, k: String): (Z, Seq[Z]) = dez.seq(z, k)

		override protected final def eoz(z: Z): Boolean = dez.eoz(z)

		def this() = this(
			Dez.this match {
				case child: Child => child.base
				case root => root
			}
		)

		override def put(z: Z, k: String, v: Z): Z = dez.put(z, k, v)

		override def put(z: Z, k: String, v: Seq[Z]): Z = dez.put(z, k, v)

		def include[T](from: Dez[Z]#De[T]): De[T] = {

			//			require(!from.isInstanceOf[Child], "SAN; we probably don't care TBH")

			// TODO; check to be sure we're not importing stuff ... somehow ...
			//require(dez eq from.host)
			from.asInstanceOf[De[T]]
		}

		override protected def newZInstance(): Z = dez.newZInstance()
	}

	case class DeFactory[O] private(f: Z => (Z, O))

	/**
	 * partial or complete trait to build a parser thing.
	 *
	 * TODO; do real/fake ones (somehow) and make it obvious which are which
	 */
	case class DeRecord[O: ClassTag] private(
																						i: (Z, O) => Z,
																						uses: Set[De[_]],
																						f: DeFactory[O]
																					)(
																						private[Dez] val ir: DeRecord[O] => Dez.IR
																					) extends De[O] {
		def encode(o: O): Z = enc(o)

		final private[Dez] def enc(value: O): Z = i(newZInstance(), value)

		/**
		 * decode a record from a container assuming that the container is empty afterwards
		 */
		def decode(z: Z): O = {
			val (n, o) = dec(z)
			assume(eoz(n))
			o
		}

		final private[Dez] def dec(value: Z): (Z, O) = f.f(value)

		def kind: Class[_] = implicitly[ClassTag[O]].runtimeClass
	}

	private class DeField private[Dez](key: String) {
		private implicit class extendDe[T: ClassTag](de: De[T]) {
			def toArg(name: String): (String, Dez.IR) = {
				TODO("removed dead arg")
				require(name == key)
				de match {
					case record: DeRecord[T] =>
						key -> record.ir(record)
					case atomic: DeAtomic[T] =>
						name -> Dez.IR.Atomic(atomic)
				}
			}
		}

		/**
		 * @param dec is the decomposer for this field
		 */
		private[Dez] def field[O: ClassTag, T: De : ClassTag](dec: O => T): P[O, DeHandle[T, O], DeFactory, DeRecord] =
			new P[O, DeHandle[T, O], DeFactory, DeRecord] {
				private val deScript: De[T] = implicitly[De[T]]

				private val handle: DeHandle.mem[T, O] = DeHandle.mem[T, O](key)

				val enc: (Z, O) => Z =
					deScript match {
						case atomic: DeAtomic[T] =>
							(i: Z, o: O) =>
								atomic(i, key, dec(o))

						case record: DeRecord[T] =>
							(i: Z, o: O) => {
								implicit val t: DeRecord[T] = record
								Dez.this.put(i, key, encode(dec(o)))
							}
					}

				override def map(f: DeHandle[T, O] => DeFactory[O]): DeRecord[O] =
					DeRecord(
						enc,
						Set(deScript),
						f(handle)
					)(
						(de: DeRecord[O]) =>
							Dez.IR.Record(de.name, List(deScript.toArg(key)))
					)

				override def flatMap(f: DeHandle[T, O] => DeRecord[O]): DeRecord[O] =
					f(handle) match {
						case re@DeRecord(a, u, f) =>
							DeRecord(
								(i: Z, o: O) => a(enc(i, o), o),
								u + deScript,
								f
							)(
								(de: DeRecord[O]) =>
									re.ir(re) match {
										case Dez.IR.Record(name, args) =>
											require(name == de.name)
											Dez.IR.Record(de.name, deScript.toArg(key) :: args)
									}
							)
					}
			}

		/**
		 * @param dec is the decomposer for this field
		 */
		private[Dez] def array[O: ClassTag, T: DeRecord : ClassTag](dec: O => Seq[T]): P[O, DeHandle[Seq[T], O], DeFactory, DeRecord] =
			new P[O, DeHandle[Seq[T], O], DeFactory, DeRecord] {
				private val deScript: DeRecord[T] = implicitly[DeRecord[T]]
				private val handle: DeHandle.seq[T, O] = DeHandle.seq[T, O](key)

				override def map(f: DeHandle[Seq[T], O] => DeFactory[O]): DeRecord[O] =
					DeRecord(
						(i: Z, o: O) => {

							// get the value
							val t: Seq[T] = dec(o)

							// set/put key:value into i
							// ... and return that ...
							Dez.this.put(i, key, t.map(encode[T]))
						},
						Set(deScript),
						f(handle)
					)(
						(de: DeRecord[O]) =>
							Dez.IR.Record(de.name, List(key -> deScript.ir(deScript)))
					)

				override def flatMap(f: DeHandle[Seq[T], O] => DeRecord[O]): DeRecord[O] = {
					TODO("remove uses")
					f(handle) match {
						case re@DeRecord(a, u, f) =>
							DeRecord(
								(i: Z, o: O) => {

									// get the value
									val t: Seq[T] = dec(o)

									// set/put key:value into i
									val n: Z = Dez.this.put(i, key, t.map(encode[T]))

									// continue
									a(n, o)
								},

								u + deScript,
								f
							)(
								(de: DeRecord[O]) =>
									re.ir(re) match {
										case Dez.IR.Record(name, args) =>
											require(name == de.name)
											Dez.IR.Record(de.name, deScript.toArg(key) :: args)
									}
							)
					}
				}
			}
	}

	object DeHandle {

		abstract class basic[F, O] extends DeHandle[F, O] {
			val key: String
			val g: Z => (Z, F)

			override def map(f: F => O): DeFactory[O] = {

				// so I want/need this signature
				// let's see where we can go ...
				val f0: Z => (Z, O) = {
					// i can declare a function that takes a Z
					(z: Z) => {
						// i can use my g() to get the next Z and my field/F
						val (nextZ, myF) = g(z)

						// now i can use my f to get the out:O
						val out: O = f(myF)

						// merge the two and return
						nextZ -> out
					}
				}

				// f0 now has the correct type
				DeFactory(f0)
			}

			override def flatMap(f: F => DeFactory[O]): DeFactory[O] = {

				// so I want/need this signature
				// let's see where we can go ...
				val f0: Z => (Z, O) = {
					// i can declare a function that takes a Z
					(z: Z) => {
						// i can use my g() to get the next Z and my field/F
						val (nextZ, myF) = g(z)

						// now ... i can use the myF to get the next factory
						val nextF: DeFactory[O] = f(myF)

						// the factory has f2/ the final f!
						val f1: Z => (Z, O) = nextF.f

						// use the nextZ in it and return that value
						f1(nextZ)
					}
				}

				// f0 now has the correct type
				DeFactory[O](f0)
			}
		}

		case class seq[E: DeRecord, O](key: String) extends basic[Seq[E], O] {
			type T = Seq[E]
			val g: Z => (Z, T) =
				implicitly[DeRecord[E]] match {
					case record: DeRecord[E] =>
						(z: Z) =>
							Dez.this.seq(z, key).mapr {
								(_: Seq[Z]).map {
									z: Z =>
										val (e, o) = record.dec(z)
										assume(eoz(e))
										o
								}
							}
				}
		}

		case class mem[T: De, O](key: String) extends basic[T, O] {
			override val g: Z => (Z, T) =
				implicitly[De[T]] match {
					case atomic: DeAtomic[T] => atomic(_: Z, key)
					case record: DeRecord[T] =>
						(z: Z) => {
							val (next, mine) = Dez.this.get(z, key)
							val out: T =
								record
									.dec(mine)
									.forl((v: Z) => assume(eoz(v)))
							next -> out
						}
				}
		}

	}

}

