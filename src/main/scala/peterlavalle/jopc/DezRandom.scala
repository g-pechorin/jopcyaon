package peterlavalle.jopc

import java.util.Random

class DezRandom(range: Int) extends Dez.Atomic[Random] with Dez.Alt[Random] {
	val chars = (('A' to 'Z') ++ ('a' to 'z') ++ ('0' to '9') ++ "\\\"`-=[];'#,./|<>?:@~{}+_)(*&^%$£!¬").distinct

	implicit val dezBoolean: De[Boolean] =
		deAtomic(
			(i: Random, key: String, t: Boolean) => newZInstance
		)(
			(z: Random, k: String) =>
				z -> z.nextBoolean()
		)

	/**
	 * a stream of values lazily created. it'd be nice iff (somehow) we/i could remove the T:Recpord
	 */
	def stream[T: DeRecord](random: Random): Stream[T] = Stream.continually(decode[T](random))

	implicit val dezByte: De[Byte] =
		deAtomic(
			(i: Random, key: String, t: Byte) => newZInstance
		)(
			(z: Random, k: String) =>
				z -> z.nextFullByte()
		)

	implicit val dezLong: De[Long] =
		deAtomic(
			(i: Random, key: String, t: Long) => newZInstance
		)(
			(z: Random, k: String) =>
				z -> z.nextLong()
		)

	implicit val dezShort: De[Short] =
		deAtomic(
			(i: Random, key: String, t: Short) => newZInstance
		)(
			(z: Random, k: String) =>
				z -> z.nextFullShort()
		)

	implicit class extendRandom(random: Random) {
		def nextFullByte(): Byte =
		// mild hack; won't hit Byte.MinValue ever
			(random.nextInt(Byte.MaxValue) - random.nextInt(Byte.MaxValue))
				.toByte

		def nextFullShort(): Short =
		// mild hack; won't hit Short.MinValue ever
			(random.nextInt(Short.MaxValue) - random.nextInt(Short.MaxValue))
				.toShort

		def nextN[N](limit: Int)(f: Random => N): List[N] =
			(0 until random.nextInt(limit))
				.map(i => nextRandom(i))
				.map(f(_: Random))
				.toList

		def nextRandom(seed: Long): Random = new Random(seed ^ random.nextLong())

		def nextE[E](from: Seq[E]): E =
			from(random.nextInt(from.length))
	}

	override def decLabel(r: Random, a: List[label]) = r -> r.nextInt(a.size)

	implicit val dezDouble: De[Double] = {
		deAtomic {
			(i: Random, key: String, t: Double) => newZInstance
		} {
			(z: Random, k: String) =>
				z -> ((z.nextDouble() * Double.MinValue) + (z.nextDouble() * Double.MaxValue))
		}
	}

	override def put(z: Random, k: String, v: Random): Random = newZInstance

	implicit val dezChar: DeAtomic[Char] =
		deAtomic[Char] {
			(i: Random, key: String, t: Char) => newZInstance
		} {
			(z: Random, k: String) =>
				z -> z.nextE(chars)
		}
	implicit lazy val dezString: De[String] =
		deAtomic[String] {
			(i: Random, key: String, t: String) => newZInstance
		} {
			(z: Random, k: String) =>
				z -> new String(
					z.nextN(range)(_.nextE(chars))
						.toArray
				)
		}
	implicit val dezFloat: De[Float] =
		deAtomic[Float] {
			(i: Random, key: String, t: Float) => newZInstance
		} {
			(z: Random, k: String) =>
				z -> {
					z.nextFloat() * Float.MinValue + z.nextFloat() * Float.MaxValue
				}
		}

	implicit val dezInt: De[Int] =
		deAtomic[Int] {
			(i: Random, key: String, t: Int) => newZInstance
		} {
			(z: Random, k: String) =>
				z -> new Random(z.nextLong() ^ k.hashCode.toLong).nextInt()
		}

	override protected def newZInstance(): Random = throw new RandomCantEncodeException()

	override def put(z: Random, k: String, v: Seq[Random]): Random = newZInstance

	override def encLabel(z: Random, a: List[label], i: Int) = newZInstance

	protected override def eoz(z: java.util.Random): Boolean = true

	protected def get(random: java.util.Random, key: String): (java.util.Random, java.util.Random) =
		random -> new Random(random.nextLong() ^ key.hashCode.toLong)

	protected def seq(random: java.util.Random, key: String): (java.util.Random, Seq[java.util.Random]) =
		random -> {
			(0 until random.nextInt(range))
				.map(
					i =>
						new Random(i.toLong ^ random.nextLong() ^ key.hashCode.toLong)
				)
		}

	class RandomCantEncodeException() extends Exception

}
