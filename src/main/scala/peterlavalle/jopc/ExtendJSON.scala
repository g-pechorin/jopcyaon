package peterlavalle.jopc

import org.json.{JSONArray, JSONObject}
import peterlavalle.jopc.include.deepEq

trait ExtendJSON {

	implicit class extendJSONObject(self: JSONObject) {

		def deepEqu(them: JSONObject): Boolean = {
			val selfKeys: List[String] = self.toListOfKeys
			val themKeys: List[String] = them.toListOfKeys
			selfKeys.foldLeft(selfKeys == themKeys) {
				case (false, _) => false
				case (_, next) =>
					import include._
					deepEq(
						(self.optJSONObject(next), them.optJSONObject(next)),
						(self.optJSONArray(next), them.optJSONArray(next)),
						(self.get(next).toString, them.get(next).toString)
					)
			}
		}

		def toListOfKeys: List[String] = toSetOfKeys.toList.sorted

		def toSetOfKeys: Set[String] = self.keySet().toArray.toList.map((_: AnyRef).asInstanceOf[String]).toSet

		def putChar(key: String, value: Char): JSONObject = self.put(key, new String(Array(value)))

		def putByte(key: String, value: Byte): JSONObject = self.put(key, value.toInt)

		def putShort(key: String, value: Short): JSONObject = self.put(key, value.toInt)

		def getChar(key: String): Char = {
			// get a string in a way that'll 'splode if the string isn't exactly one char long

			val List(value) = self.getString(key).toList
			value
		}

		def getShort(key: String): Short = self.getInt(key).toShort

		def getByte(key: String): Byte = self.getInt(key).toByte

		def /(path: String): JSONObject =
			if (path.isEmpty)
				self
			else {
				val head: String = path.takeWhile('/' != (_: Char))

				self.extJSONObject(head, new JSONObject()) / path.dropWhile('/' != (_: Char)).drop(1)
			}

		def extJSONArray(key: String, value: => JSONArray = new JSONArray()): JSONArray = {

			val out: JSONArray = self.optJSONArray(key)

			if (null != out)
				out
			else {
				self.put(key, value: JSONArray)
					.get(key)
					.asInstanceOf[JSONArray]
			}
		}

		def extJSONObject(key: String, value: => JSONObject = new JSONObject()): JSONObject = {

			val out: JSONObject = self.optJSONObject(key)

			if (null != out)
				out
			else {
				self.put(key, value: JSONObject)
					.get(key)
					.asInstanceOf[JSONObject]
			}
		}
	}


	implicit class extendJSONArray(self: JSONArray) {

		def toStreamOf[T](map: (JSONArray, Int) => T): Stream[T] = {
			(0 until self.length())
				.toStream
				.map(map(self, _: Int))
		}

		def deepEqu(them: JSONArray): Boolean =
			(0 until self.length()).foldLeft(self.length() == them.length()) {
				case (false, _) => false
				case (_, next) =>
					deepEq(
						(self.optJSONObject(next), them.optJSONObject(next)),
						(self.optJSONArray(next), them.optJSONArray(next)),
						(self.get(next).toString, them.get(next).toString)
					)
			}
	}

}
