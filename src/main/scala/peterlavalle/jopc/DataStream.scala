package peterlavalle.jopc

import java.io._


case class DataStream(data: DataStream.Chunk) {
	lazy val isEmpty: Boolean = data.length == 0

	def append(f: DataOutputStream => Unit): DataStream = {
		val outputStream = new ByteArrayOutputStream()
		f(new DataOutputStream(outputStream))
		new DataStream(data append outputStream.toByteArray)
	}

	def remove[O](f: DataInputStream => O): (DataStream, O) = {
		val inputStream = data.consume
		val out: O = f(new DataInputStream(inputStream))
		new DataStream(inputStream()) -> out
	}

	def inhume(s: OutputStream): Unit = {
		inhume(new DataOutputStream(s))
	}

	def inhume(s: DataOutputStream): Unit = {
		s.writeInt(data.length)
		data.write(s)
	}

	object seq {
		def inhume[E](seq: Seq[E])(f: (DataOutputStream, E) => Unit): DataStream =
			seq.foldLeft(append((_: DataOutputStream).writeInt(seq.length))) {
				case (left: DataStream, next) =>
					left.append(f(_: DataOutputStream, next))
			}

		def exhume[E](f: DataInputStream => E): (DataStream, Seq[E]) = {
			// read the length
			val (next: DataStream, len) = remove((_: DataInputStream).readInt())
			// read the items
			(0 until len).foldLeft(next -> List[E]()) {
				case ((data: DataStream, done), _: Int) =>

					// remote the item
					data.remove(f)
						// manipulate the return tuple
						.mapr(done :+ (_: E))
			}
		}
	}

}

object DataStream {

	val Blank: DataStream = new DataStream(Nol)

	def exhume(stream: DataInputStream): DataStream = {
		val size: Int = stream.readInt()
		val data: Array[Byte] = stream.readBytes(size)
		require(size == data.length, size + " != " + data.length)
		new DataStream(Nol append data)
	}

	private def chunks(data: Chunk*) = {
		val blobs: Seq[Blob] = data.flatMap {
			case Nol => Nil
			case Chunks(data) => data
			case blob: Blob => Seq(blob)
		}
		if (blobs.isEmpty)
			Nol
		else
			Chunks(blobs)
	}

	sealed trait Chunk {


		lazy val length: Int =
			this match {
				case Blob(data, offset) => data.length - offset
				case Chunks(data) => data.map(_.length).sum
				case Nol => 0
			}

		def write(outputStream: OutputStream): Unit =
			this match {
				case Blob(data, offset) => outputStream.write(data, offset, length)
				case Chunks(data) => data.foreach(_.write(outputStream))
				case Nol => {}
			}

		def append(data: Array[Byte]): Chunk =
			this match {
				case Nol =>
					if (data.isEmpty) Nol else Blob(data, 0)
				case blob: Blob =>
					chunks(blob, Nol append data)
				case Chunks(lead) =>
					chunks(lead :+ (Nol append data): _ *)
			}

		def consume: InputStream with (() => Chunk) = {
			this match {
				case Blob(data, offset) =>

					new InputStream with (() => Chunk) {
						var pos: Int = offset
						require(0 <= pos && pos < data.length)
						val inner = new ByteArrayInputStream(data, offset, length)

						override def read(): Int = {
							if (pos != data.length)
								pos += 1
							inner.read()
						}

						override def apply(): Chunk =
							if (pos < data.length)
								Blob(data, pos)
							else
								Nol

					}
				case Chunks(data) =>

					def loop(data: Stream[Blob]): InputStream with (() => Chunk) =
						data match {
							case Stream.Empty => Nol.consume
							case head #:: tail =>
								new InputStream with (() => Chunk) {
									val self = head.consume
									val next = loop(tail)

									override def read(): Int = {
										val out = self.read()
										if (-1 == out)
											next.read()
										else
											out
									}

									override def apply(): Chunk =
										chunks(self.apply(), next.apply())
								}
						}

					loop(data.toStream)
				case Nol =>
					new InputStream with (() => Chunk) {
						override def read(): Int = -1

						override def apply(): Chunk = Nol
					}
			}
		}
	}

	private case class Blob(data: Array[Byte], offset: Int) extends Chunk

	private case class Chunks(data: Seq[Blob]) extends Chunk

	case object Nol extends Chunk

}
