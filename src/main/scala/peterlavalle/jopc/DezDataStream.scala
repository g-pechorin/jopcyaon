package peterlavalle.jopc

import java.io.{DataInputStream, DataOutputStream, InputStream, OutputStream}

class DezDataStream
	extends Dez.Atomic[DataStream]
		with Dez.Alt[DataStream] {


	def read[T: DeRecord](in: InputStream, sleep: Long = 10)(into: T => Unit): AutoCloseable =
		new Thread() with AutoCloseable {
			private val inputStream = new DataInputStream(in)

			object lock {
				var live = true
			}

			override def run(): Unit = {
				lock.synchronized {
					lock.notifyAll()
				}

				while (lock.synchronized(lock.live))
					if (0 == inputStream.available())
						Thread.sleep(sleep)
					else
						into(decode[T](DataStream.exhume(inputStream)))

				// close up stuff
				inputStream.close()
				into match {
					case into: (T => Unit) with AutoCloseable =>
						into.close()
					case _ =>
				}
			}

			lock.synchronized {
				start()
				lock.wait()
			}

			override def close(): Unit = {
				lock.synchronized {
					require(lock.live)
					lock.live = false
				}
				join()

			}
		}


	/**
	 * build a (closeable) object to send messages into the passed output stream or close it when closed
	 */
	def send[T: DeRecord](output: OutputStream): (T => Unit) with AutoCloseable =
		new (T => Unit) with AutoCloseable {
			override def apply(t: T): Unit = {
				encode[T](t)
					.inhume(output)
			}

			override def close(): Unit = {
				(output match {
					case outputStream: DataOutputStream=> outputStream
					case _ => new DataOutputStream(output)
				}).writeInt(0)
				output.close()
			}
		}

	implicit val dezBoolean: De[Boolean] =
		deAtomic[Boolean](
			(d: DataStream, _: String, b: Boolean) =>
				d.append((_: DataOutputStream).writeBoolean(b))
		)(
			(d: DataStream, _: String) =>
				d.remove((_: DataInputStream).readBoolean())
		)

	implicit val dezByte: De[Byte] =
		deAtomic[Byte] {
			(invoice: DataStream, _: String, value: Byte) =>
				invoice.append((_: DataOutputStream).writeByte(value))
		} {
			case (data: DataStream, _: String) =>
				data.remove((_: DataInputStream).readByte())
		}

	implicit val dezChar: De[Char] =
		deAtomic[Char](
			(d: DataStream, _: String, c: Char) =>
				d.append((_: DataOutputStream).writeChar(c))
		)(
			(d: DataStream, _: String) =>
				d.remove((_: DataInputStream).readChar())
		)

	implicit val dezDouble: De[Double] =
		deAtomic[Double] {
			(invoice: DataStream, _: String, value: Double) =>
				invoice.append((_: DataOutputStream).writeDouble(value))
		} {
			case (data: DataStream, _: String) =>
				data.remove((_: DataInputStream).readDouble())
		}

	implicit val dezLong: De[Long] =
		deAtomic[Long] {
			(invoice: DataStream, _: String, value: Long) =>
				invoice.append((_: DataOutputStream).writeLong(value))
		} {
			case (data: DataStream, _: String) =>
				data.remove((_: DataInputStream).readLong())
		}

	implicit val dezShort: De[Short] =
		deAtomic[Short] {
			(invoice: DataStream, _: String, value: Short) =>
				invoice.append((_: DataOutputStream).writeShort(value))
		} {
			case (data: DataStream, _: String) =>
				data.remove((_: DataInputStream).readShort())
		}

	override def put(z: DataStream, k: String, v: DataStream): DataStream =
		z.append(v.inhume)

	override def put(z: DataStream, k: String, v: Seq[DataStream]): DataStream =
		z.seq.inhume(v) {
			case (o, s) =>
				s.inhume(o)
		}

	override def encLabel(d: DataStream, a: List[label], i: Int): DataStream =
		d.append((_: DataOutputStream).writeInt(i))

	implicit val dezInt: De[Int] =
		deAtomic[Int] {
			(invoice: DataStream, _: String, value: Int) =>
				invoice.append((_: DataOutputStream).writeInt(value))
		} {
			case (data: DataStream, _: String) =>
				data.remove((_: DataInputStream).readInt())
		}

	implicit val dezFloat: De[Float] =
		deAtomic[Float] {
			case (invoice: DataStream, _: String, value: Float) =>
				invoice.append((_: DataOutputStream).writeFloat(value))
		} {
			case (data: DataStream, _: String) =>
				data.remove((_: DataInputStream).readFloat())
		}

	implicit val dezString: De[String] =
		deAtomic[String] {
			case (invoice: DataStream, _: String, value: String) =>
				val bytes: Array[Byte] = value.getBytes("UTF-8")
				require(!bytes.contains(0))
				invoice.append((_: DataOutputStream).write(bytes ++ Array(0.toByte)))
		} {
			case (data: DataStream, _: String) =>
				data.remove {
					in: DataInputStream =>
						new String(
							Stream.continually(in.read())
								.takeWhile(0 != (_: Int))
								.map((_: Int).toByte)
								.toArray,
							"UTF-8"
						)
				}
		}

	override def decLabel(d: DataStream, a: List[label]): (DataStream, Int) =
		d.remove((_: DataInputStream).readInt())

	protected def eoz(z: DataStream): Boolean = z.isEmpty

	protected def get(z: DataStream, k: String): (DataStream, DataStream) =
		z.remove(DataStream.exhume)

	protected def seq(z: DataStream, k: String): (DataStream, Seq[DataStream]) =
		z.seq.exhume(DataStream.exhume)

	override protected def newZInstance(): DataStream = DataStream.Blank
}
