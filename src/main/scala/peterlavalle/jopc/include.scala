package peterlavalle.jopc

import org.json.{JSONArray, JSONObject}

object include extends peterlavalle.include
	with include {
	final def deepEq(o: => (JSONObject, JSONObject), a: => (JSONArray, JSONArray), v: => (String, String)): Boolean = (o) match {
		case (self: JSONObject, them: JSONObject) =>
			self deepEqu them
		case (null, null) =>
			(a) match {
				case (self: JSONArray, them: JSONArray) =>
					self deepEqu them
				case (null, null) =>
					val (l, r) = v
					l == r
			}
	}
}

trait include extends ExtendJSON

