package peterlavalle.jopc

import org.json.{JSONArray, JSONObject}

class DezJSONObject extends Dez.Atomic[JSONObject] with Dez.Alt[JSONObject] {

	/**
	 * test if a container is "at end"
	 *
	 * used to verify that fields are decomposed properly - for Random just return true always
	 */
	override def eoz(z: org.json.JSONObject): Boolean = {
		// we can't actually setup to check this without generating clones of JSON objects
		// ... which would produce allocations
		// ... and would prevent the thing from ignoring fields
		true
	}

	override def put(z: JSONObject, k: String, v: JSONObject): JSONObject =
		z.put(k, v)

	override def put(z: JSONObject, k: String, v: Seq[JSONObject]): JSONObject =
		z.put(k, v.foldLeft(new JSONArray())((_: JSONArray) put (_: JSONObject)))

	implicit lazy val dezInt: De[Int] =
		deAtomic[Int] {
			case (i: JSONObject, k: String, t: Int) => i.put(k, t)
		} {
			(j: JSONObject, k: String) => j -> j.getInt(k)
		}

	implicit lazy val dezFloat: De[Float] =
		deAtomic[Float] {
			case (i: JSONObject, k: String, t: Float) => i.put(k, t)
		} {
			(j: JSONObject, k: String) => j -> j.getFloat(k)
		}

	implicit lazy val dezString: De[String] =
		deAtomic[String] {
			case (i: JSONObject, k: String, t: String) => i.put(k, t)
		} {
			(j: JSONObject, k: String) => j -> j.getString(k)
		}

	override def encLabel(json: JSONObject, list: List[label], kind: Int) =
		json.put(".kind.", list(kind).name)

	override def decLabel(json: JSONObject, list: List[label]) = {
		require(2 == json.keySet().size, "SAN check")
		val kind: Int = list.map(_.name).indexOf(json.getString(".kind."))
		require(0 <= kind)
		json -> kind
	}

	/**
	 * dez: here's the secret - De2 and newField treated V differently.
	 *
	 * retrieves a sub-component from Z for the given field
	 *
	 * ... for JSON it would be the next Array or Object
	 * ... for Random it could be a sub-Random?
	 * ... bor bytes ... I have no idea ...
	 */
	protected def get(o: org.json.JSONObject, k: String): (org.json.JSONObject, org.json.JSONObject) = o -> o.getJSONObject(k)

	implicit lazy val dezLong: De[Long] =
		deAtomic[Long] {
			case (i: JSONObject, k: String, t: Long) => i.put(k, t)
		} {
			(j: JSONObject, k: String) => j -> j.getLong(k)
		}

	implicit lazy val dezByte: De[Byte] =
		deAtomic[Byte] {
			case (i: JSONObject, k: String, t: Byte) => i.putByte(k, t)
		} {
			(j: JSONObject, k: String) => j -> j.getByte(k)
		}

	implicit lazy val dezShort: De[Short] =
		deAtomic[Short] {
			case (i: JSONObject, k: String, t: Short) => i.putShort(k, t)
		} {
			(j: JSONObject, k: String) => j -> j.getShort(k)
		}

	implicit lazy val dezDouble: De[Double] =
		deAtomic[Double] {
			case (i: JSONObject, k: String, t: Double) => i.put(k, t)
		} {
			(j: JSONObject, k: String) => j -> j.getDouble(k)
		}

	implicit lazy val dezChar: De[Char] =
		deAtomic[Char] {
			case (i: JSONObject, k: String, t: Char) => i.putChar(k, t)
		} {
			(j: JSONObject, k: String) => j -> j.getChar(k)
		}

	implicit lazy val dezBoolean: De[Boolean] =
		deAtomic[Boolean] {
			case (i: JSONObject, k: String, t: Boolean) => i.put(k, t)
		} {
			(j: JSONObject, k: String) => j -> j.getBoolean(k)
		}

	/**
	 * dez: here's the secret - De2 and newField treated V differently.
	 */
	override protected def seq(json: org.json.JSONObject, key: String): (org.json.JSONObject, Seq[org.json.JSONObject]) =
		json -> json.getJSONArray(key).toStreamOf((_: JSONArray) getJSONObject (_: Int))

	override protected def newZInstance(): JSONObject = new JSONObject()
}
