

# JOPC

This project became two things (sorry) which both delight me to no end.

## YAON (Yet Another Object Notation)

This is a silly wrapping around `org.json.JSONObject` that lets me read/write from a YAML like format.

It's not YAML.

It never will be YAML.

It's just more succinct and prettier than `.json` tends to be - so I love it.

I pronounce it "yawn" and it's a JSON subset that;

- looks a bit like YAML
- (probably) isn't actually YAML (sorry)
- smaller than JSON
	- easier for humans/me to mentally parse
- more-stable text
	- more-strict indentation policy
	- stores {k:v} in alphabetical order
	- less version-control "thrashing"
- has a "prefix" at the file head
	- ... to mark ... version?
	- it itself *could be* JSON so ... go nuts

## Dez

This is a "like-ORM" but for Scala objects coming in/out of `org.json.JSONObject`, `Array[Byte]`, `java.util.Random` or really anything.
The encoders are written separatly from the usages (and can be tested like that) while using an (IMO) almost-perfect syntax with minimal dynamic shenanigans.
AFAIK; no reflection of the sort Android struggles with is used, so, this should survive the Dalvik converters.<a id='f_link1' name='f_link1'/><sup>[1](#f_note1)</sup>

I'll probably add JDBC implementations someday.
For now - I'm build a Python encoder/decoder for it and using it to drive a visual editor.

<a id='f_link1' name='f_link1'/><sup>[1](#f_note1)</sup>: Scala is flakey on Android last I checked ... so this might not be true anymore.

### Frontend

To decode a piece of data, define a `trait` in terms of `[Z]` which `extends Dez[Z]` and provices an `implicit De[Y]` for your type `Y`.
The [`Foo`](peterlavalle.jopc.Foo) class probably shows this most simply.
In truth - `Foo` should probably extend `Dez.Atomics[Z]` to get consistent `De[Int]` and `De[Float]` values.
If you want to use collections of primitive objects (`Seq[String]`?) you should extend/mixin `Dez.Atomics[Z]` since it's got `box()` and `boi()` methods to use for that.<a id='f_link2' name='f_link2'/><sup>[2](#f_note2)</sup>
There's also `Dez.Alt[Z]` if you want multiple alternatives and `Dez.Ext[Z]` if you want `Map[_, _]` or `Set[_]` or such.

All of these can be mixed-in together as needed, but, `Dez.Alt[Z]` and `Dez.Atomics[Z]` impose some virtual functions on the backend.
(My backends already satisfy these requirements though - so use whatever you need)

<a id='f_link2' name='f_link2'/><sup>[2](#f_note2)</sup>: I'm trying to use three-letter names for these, but, am tired as I write this.

### Backend

In terms of the backend, they're all defined by extending [`Dez[Z]`](src/main/scala/peterlavalle/jopc/Dez.scala) for some container `Z`.
Most usages should extend `Dez.Atomics[Z]` to provide consistent handling of the JDK primitives and string values and/or `Dez.Alt[Z]` to handle data with multiple types.<a id='f_link3' name='f_link3'/><sup>[3](#f_note3)</sup>
[`DezDataStream`](src/main/scala/peterlavalle/jopc/DezDataStream.scala) is probably the simplest "backend" to understand.
It exploits the fact that the fields arrive in-order and it ignores the "key" values.
The file is < 125 lines long (for now) and most of that is repetitive cases for JDK primitve types.<a id='f_link4' name='f_link4'/><sup>[4](#f_note4)</sup>
[`DezJSONObject`](src/main/scala/peterlavalle/jopc/DezJSONObject.scala) needs to engage in some extra silliness for the alt types, and, some `implicit` class extensions ... so ... it's more complicated.

<a id='f_link3' name='f_link3'/><sup>[3](#f_note3)</sup>: Going forward ... I would be open to doing the alts as "user space only" ones ... but how is a bit beyond me.

<a id='f_link4' name='f_link4'/><sup>[4](#f_note4)</sup>: YES - I'm cheating with "125" because it's built on [`DataStream`](src/main/scala/peterlavalle/jopc/DataStream.scala) which is an abstraction across another 57 lines to make `DataInputStream` and `DataOutputStream` sane.

----
